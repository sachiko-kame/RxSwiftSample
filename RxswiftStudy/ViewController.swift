//
//  ViewController.swift
//  RxswiftStudy
//
//  Created by 水野祥子 on 2017/11/03.
//  Copyright © 2017年 sachiko. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

//参考: https://qiita.com/fumiyasac@github/items/90d1ebaa0cd8c4558d96#_reference-b4926d65d5112dd439a6

class ViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextField!
    
    @IBOutlet weak var sampleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var sampleButton: UIButton!
    
    var username: String = "" {
        didSet {
            print(username)
        }
    }
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nameObservable: Observable<String?> = nameTextField.rx.text.asObservable()
        let commentObservable: Observable<String?> = commentTextField.rx.text.asObservable()
        
        
        let CommentandNameObservable: Observable<String?> = Observable.combineLatest(
            //引数
            nameObservable,
            commentObservable
            //処理返す値
        ) { (string1: String?, string2: String?) in
            return string1! + string2!
        }
        
        nameObservable
            .bind(to: nameLabel.rx.text)
            .disposed(by: disposeBag)
        
        nameObservable
            .subscribe({ text in
//                print(text.debugDescription)
                self.username = text.element!!
            }).disposed(by: disposeBag)
        

        CommentandNameObservable
            .bind(to: sampleLabel.rx.text)
            .disposed(by: disposeBag)
        
        sampleButton.rx.tap.subscribe(onNext: { (nothing: Void) in
           print("🌹")
        }).disposed(by: disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
